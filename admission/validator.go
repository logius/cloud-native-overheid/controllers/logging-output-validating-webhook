package admission

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"

	loggingv1beta1 "github.com/banzaicloud/logging-operator/pkg/sdk/logging/api/v1beta1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// LoggingOutputValidator validates Banzai Logging Output
type LoggingOutputValidator struct {
	Client  client.Client
	decoder *admission.Decoder
}

func (v *LoggingOutputValidator) Handle(ctx context.Context, req admission.Request) admission.Response {
	loggingOutput := &loggingv1beta1.Output{}

	logger := log.Log.WithName("validator")

	logger.Info(fmt.Sprintf("Handling new validation request: %s", req.AdmissionRequest.Name))

	logger.Info("Decoding request")
	err := v.decoder.Decode(req, loggingOutput)
	if err != nil {
		logger.Info(fmt.Sprintf("Error when decoding request. %s in namespace %s. Request denied", req.AdmissionRequest.Name, req.AdmissionRequest.Namespace))
		return admission.Errored(http.StatusBadRequest, err)
	}

	if loggingOutput.Spec.ElasticsearchOutput != nil {

		if response := v.validateElasticSearchOutput(ctx, loggingOutput); !response.Allowed {
			return response
		}
	}

	logger.Info(fmt.Sprintf("Rule %s in namespace %s successfully admitted.", req.AdmissionRequest.Name, req.AdmissionRequest.Namespace))
	return admission.Allowed("Rule admitted by Logging Output validating webhook.")
}

func (v *LoggingOutputValidator) validateElasticSearchOutput(ctx context.Context, loggingOutput *loggingv1beta1.Output) admission.Response {
	es := loggingOutput.Spec.ElasticsearchOutput

	// check proper host
	if len(es.Hosts) > 0 && es.Host == "" {
		return admission.Allowed("OK").WithWarnings("cannot validate multiple hosts")
	}
	if es.Host == "" {
		return admission.Denied(fmt.Sprintf("elasticsearch.host should be specified"))
	}
	if _, err := net.LookupIP(es.Host); err != nil {
		return admission.Denied(fmt.Sprintf("ElasticSearch host %q could not be resolved", es.Host))
	}

	if !managedServiceAccount(loggingOutput) {
		// check password
		password, response := v.getOutputPassword(ctx, loggingOutput)
		if !response.Allowed {
			return response
		}

		// check datastream
		if es.DataStreamName != "" {

			// check for missing data_stream_enable
			if es.DataStreamEnable == nil || *es.DataStreamEnable == false {
				return admission.Denied("elasticsearch.data_stream_enable should be true")
			}

			// check access to datastream using credentials
			if response := checkDatastream(es.Host, es.DataStreamName, es.User, password); !response.Allowed {
				return response
			}
		}
	}
	return admission.Allowed("OK")
}

func managedServiceAccount(loggingOutput *loggingv1beta1.Output) bool {
	for key, value := range loggingOutput.GetAnnotations() {
		if key == "logging.lpc.logius.nl/managedServiceAccount" {
			b, _ := strconv.ParseBool(value)
			return b
		}
	}
	return false
}

// Output may refer to a secret for the password. Read this secret and return the password value.
func (v *LoggingOutputValidator) getOutputPassword(ctx context.Context, loggingOutput *loggingv1beta1.Output) (string, admission.Response) {
	es := loggingOutput.Spec.ElasticsearchOutput

	if es.Password != nil {

		if es.Password.ValueFrom != nil && es.Password.ValueFrom.SecretKeyRef != nil {

			objectKey := types.NamespacedName{
				Name:      es.Password.ValueFrom.SecretKeyRef.Name,
				Namespace: loggingOutput.ObjectMeta.Namespace,
			}
			var secret v1.Secret
			if err := v.Client.Get(ctx, objectKey, &secret); err != nil {
				if errors.IsNotFound(err) {
					return "", admission.Denied(fmt.Sprintf("Secret %q does not exist in namespace %q", objectKey.Name, objectKey.Namespace))
				}
				return "", admission.Errored(http.StatusBadRequest, err)
			}
			passwordData, ok := secret.Data[es.Password.ValueFrom.SecretKeyRef.Key]
			if !ok {
				return "", admission.Denied(fmt.Sprintf("Secret %q does not have key %q", objectKey.Name, es.Password.ValueFrom.SecretKeyRef.Key))
			}
			return string(passwordData), admission.Allowed("OK")
		}
		if es.Password.Value != "" {
			return es.Password.Value, admission.Allowed("OK")
		}
	}

	return "", admission.Denied("Missing password for ElasticSearch output")
}

func checkDatastream(elasticHost string, datastream string, userName string, password string) admission.Response {
	logger := log.Log.WithName("datastream")

	url := fmt.Sprintf("https://%s/_data_stream/%s", elasticHost, datastream)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		logger.Info(fmt.Sprintf("Request denied: bad request to %s with %s.", elasticHost, datastream))
		return admission.Errored(http.StatusBadRequest, err)
	}
	req.SetBasicAuth(userName, password)

	logger.Info(fmt.Sprintf("Set basic auth for %s.", elasticHost))

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		logger.Info(fmt.Sprintf("Http request failed to %s with %s as datastream. %v", elasticHost, datastream, err))
		return admission.Errored(http.StatusBadRequest, err)
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case 401:
		return admission.Denied(fmt.Sprintf("user %q is not authorized", userName))
	case 403, 404:
		return admission.Denied(fmt.Sprintf("user %q has no access to datastream %q", userName, datastream))
	}
	if res.StatusCode != 200 {
		return admission.Errored(http.StatusBadRequest, fmt.Errorf("status code: %d", res.StatusCode))
	}

	logger.Info("Request to elastic succesful, will now attempt to read responsedata")

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return admission.Errored(http.StatusBadRequest, err)
	}

	type DataStreamList struct {
		DataStreams []struct {
			Name string `json:"name"`
		} `json:"data_streams"`
	}

	var datastreamList DataStreamList
	if len(responseData) > 0 {
		if err = json.Unmarshal(responseData, &datastreamList); err != nil {
			return admission.Errored(http.StatusBadRequest, err)
		}
	}
	if len(datastreamList.DataStreams) == 0 {
		logger.Info(fmt.Sprintf("Failed. User %q has no access to datastream %q", userName, datastream))
		return admission.Denied(fmt.Sprintf("user %q has no access to datastream %q", userName, datastream))
	}
	return admission.Allowed("OK")
}

func (v *LoggingOutputValidator) InjectDecoder(d *admission.Decoder) error {
	v.decoder = d
	return nil
}
