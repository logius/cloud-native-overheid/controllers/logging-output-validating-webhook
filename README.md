## Logging Output Webhook Validator
This project creates a webhook validator for the Banzai Logging Output. 

The Logging Output is a CRD defined by the Banzai Logging Operator at https://github.com/banzaicloud/logging-operator.

## Validation rules

The validator checks ElasticSearch Outputs:
- ElasticSearch hostname can be resolved
- ElasticSearch credentials should be proper
- User has access to ElasticSearch API 
- User has access to DataStream 

Other outputs than ElasticSearch are not validated. 

## How was this project initialized

The operator-sdk was used for code generation:
```` 
operator-sdk init 
  --domain=logging.banzaicloud.io 
  --repo=gitlab.com/logius/cloud-native-overheid/controllers/logging-output-validating-webhook
````

The validator does not `own` the API (or CRD) as a normal operator would do, but imports the API from the Banzai project. It is therefore not possible to run `create api` generators in this project itself.

The folders `certmanager` and `webhook` have been been copied from another operator project, where this code was generated: 
````
operator-sdk create api
operator-sdk create webhook --programmatic-validation
````

`main.go` and `admission/validator.go` have been derived from `controller-runtime`: https://github.com/kubernetes-sigs/controller-runtime/blob/master/examples/builtins.

The `role.yaml` was amended to provide the webhook privileges to read secrets. This is needed to validate the credentials of the ElasticSearch account. 

## Cleanup generated artefacts

Some cleanup of generated artefacts was done to make the project easier to understand. 

The following generated code was removed:
- folders `config/manifests`, `config/scorecard` and `hack` as they are not needed in a webhook validator for external types.
- folder `config/prometheus` as we do not need monitoring on the validator.
- `Makefile` was removed as we will be using GitLab bash scripting. 
- The code for authorization of the metrics endpoint. 
- Generated code for liveness and readiness was removed from config. It did not work properly and is not required. 

The generated kustomize files have been simplified. E.g. webhook and manager have been merged. Also, some kustomize patch files are merged into the base manifest.

## Why using Operator SDK

The following blog motivates to use operator-sdk for creating a webhook: 
- https://developers.redhat.com/articles/2021/09/17/kubernetes-admission-control-validating-webhooks

Also, the kubebuilder documentation explains how to create a webhook for external types: 
- https://book.kubebuilder.io/reference/webhook-for-core-types.html
- https://github.com/kubernetes-sigs/controller-runtime/tree/master/examples/builtins

## CertManager
The webhook needs the certmanager installed in the same cluster. 

The webhook needs a PKI CA and certificate. The generated code for the certmanager works really well.

## How to deploy

Download the project  gitlab.com/logius/cloud-native-overheid/controllers/logging-output-validating-webhook

Create a new kustomization file `kustomization.yaml` to override the namespace and the image tag. 
````
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: my-namespace
resources:
- <path-to>/logging-output-validating-webhook/config/default
images:
- name: controller
  newName: logging-output-validating-webhook
  newTag: latest
````

Next run: 
`kubectl -k .` 
